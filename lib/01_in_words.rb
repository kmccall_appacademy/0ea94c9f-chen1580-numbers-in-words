class Fixnum
  def in_words
    if self < 100
      less_than_100(self)
    elsif self < 1000
      less_than_1000(self)
    elsif self < 1000000
      less_than_million(self)
    elsif self < 1_000_000_000
      less_than_billion(self)
    elsif self < 1_000_000_000_000
      less_than_trillion(self)
    else
      everything_else(self)
    end
  end

  private

  def zero_to_10(num)
    words = ["zero","one","two","three","four","five","six","seven","eight","nine","ten"]
    words[num]
  end

  def eleven_to_nineteen(num)
    words = %w[eleven twelve thirteen fourteen fifteen sixteen seventeen eighteen nineteen]
    words[num-11]
  end

  def tens(num)
    digit = num.to_s[0]
    words = %w[twenty thirty forty fifty sixty seventy eighty ninety]
    words[digit.to_i-2]
  end

  def less_than_100(num)
    if num < 11
      zero_to_10(num)
    elsif num < 20
      eleven_to_nineteen(num)
    elsif num < 100 && num % 10 == 0
      tens(num)
    else
      tens(num) + ' ' + zero_to_10(num % 10)
    end
  end

  def hundreds(num)
    first_digit = num.to_s[0]
    zero_to_10(first_digit.to_i) + ' hundred'
  end

  def less_than_1000(num)
    if num < 100
      less_than_100(num)
    elsif num % 100 == 0
      hundreds(num)
    else
      hundreds(num) + ' ' + less_than_100(num % 100)
    end
  end

  def thousands(num)
    num_string = num.to_s
    #first_digit = num.to_s[0]
    case num_string.length
    when 4
      zero_to_10(num_string[0].to_i) + ' thousand'
    when 5
      less_than_100(num_string[0..1].to_i) + ' thousand'
    when 6
      less_than_1000(num_string[0..2].to_i) + ' thousand'
    end
  end

  def less_than_million(num)
    string_length = num.to_s.length
    if num < 1000
      less_than_1000(num)
    elsif num % 10**(string_length-1) == 0
      thousands(num)
    else
      thousands(num) + ' ' + less_than_1000(num % 1000)
    end

  end

  def millions(num)
    num_string = num.to_s
    case num_string.length
    when 7
      zero_to_10(num_string[0].to_i) + ' million'
    when 8
      less_than_100(num_string[0..1].to_i) + ' million'
    when 9
      less_than_1000(num_string[0..2].to_i) + ' million'
    end
  end

  def less_than_billion(num)
    string_length = num.to_s.length
    if num < 1_000_000
      less_than_million(num)
    elsif num % 10**(string_length-1) == 0
      millions(num)
    else
      millions(num) + ' ' + less_than_million(num % 1_000_000)
    end
  end

  def billions(num)
    num_string = num.to_s
    case num_string.length
    when 10
      zero_to_10(num_string[0].to_i) + ' billion'
    when 11
      less_than_100(num_string[0..1].to_i) + ' billion'
    when 12
      less_than_1000(num_string[0..2].to_i) + ' billion'
    end
  end

  def less_than_trillion(num)
    string_length = num.to_s.length
    if num < 1_000_000_000
      less_than_billion(num)
    elsif num % 10**(string_length-1) == 0
      billions(num)
    else
      billions(num) + ' ' + less_than_billion(num % 1_000_000_000)
    end
  end

  def trillions(num)
    num_string = num.to_s
    case num_string.length
    when 13
      zero_to_10(num_string[0].to_i) + ' trillion'
    when 14
      less_than_100(num_string[0..1].to_i) + ' trillion'
    when 15
      less_than_1000(num_string[0..2].to_i) + ' trillion'
    end
  end

  def everything_else(num)
    string_length = num.to_s.length
    if num < 1000000000000
      less_than_trillion(num)
    elsif num % 10**(string_length-1) == 0
      trillions(num)
    else
      trillions(num) + ' ' + less_than_trillion(num % 1_000_000_000_000)
    end
  end

end
